# Create KVM VM using terraform

# Prerequite
Have to download terraform-provider-libvirt plugin at the following link.
* https://github.com/dmacvicar/terraform-provider-libvirt

have to copy the plugin file into .terraform.d/plugins at your home directory.

# Environment
Verified below versions
 - terraform 0.12.19
 - libvirtd 5.4.0
 - QEMU 4.0.0
 - terraform-provider-libvirt 0.6.1

# Recommended
It is better to use authentication with ssh-key.
If the using ssh-key, don't use the password.

# Terraform Output Example
```
ceph_instances_with_ipaddr = {
  "jacobbaek-ceph001" = [
    "192.168.201.18.184",
  ]
  "jacobbaek-ceph002" = [
    "192.168.201.18.185",
  ]
  "jacobbaek-ceph003" = [
    "192.168.201.18.186",
  ]
}
osp_instances_with_ipaddr = {
  "jacobbaek-osp-com001" = [
    "192.168.201.18.183",
  ]
  "jacobbaek-osp-ctl001" = [
    "192.168.201.18.180",
  ]
  "jacobbaek-osp-ctl002" = [
    "192.168.201.18.181",
  ]
  "jacobbaek-osp-ctl003" = [
    "192.168.201.18.182",
  ]
}
```

# Don't forget this
- have to use default network(fastvm-nat is not suitable)

# How to use
 0. should check and modify the variable.tf
 1. terraform init
 2. terraform plan
 3. terraform apply --auto-approve
