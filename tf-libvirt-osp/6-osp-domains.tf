resource "libvirt_cloudinit_disk" "ospcloudinit" {
  count = length(var.osp_names)
  pool = var.disk_pool

  name = "${var.prjname}-osp-cloudinit${count.index}.iso"
  user_data = data.template_file.user_data[count.index].rendered
  meta_data = data.template_file.osp_metadata[count.index].rendered
  network_config = data.template_file.osp_network_config[count.index].rendered
}

resource "libvirt_domain" "osp-instances" {
  count = length(var.osp_names)

  name = format("%s-%s", var.prjname, element(var.osp_names, count.index))
  memory = "16384"
  vcpu = 4
  cloudinit = libvirt_cloudinit_disk.ospcloudinit[count.index].id

  disk {
    volume_id = libvirt_volume.osproot[count.index].id
  }
  
  disk {
    volume_id = libvirt_volume.ospdisk[count.index].id
  }

  # deploy network
  network_interface {
    network_id = libvirt_network.deploy.id
    addresses = ["${var.deploy_addr}.${var.osp_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # storage monitor network
  network_interface {
    network_id = libvirt_network.monitor.id
    addresses = ["${var.monitor_addr}.${var.osp_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # storage network
  network_interface {
    network_id = libvirt_network.storage.id
    addresses = ["${var.storage_addr}.${var.osp_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # internal api network
  network_interface {
    network_id = libvirt_network.internal.id
    addresses = ["${var.internal_addr}.${var.osp_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # external network
  network_interface {
    network_name = "${var.prjname}-external"
    addresses = ["${var.external_addr}.${var.osp_ipnum}${count.index}"]
    wait_for_lease = false
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }

  connection {
    type = "ssh"
    user = "root"
    password = "root123"
    host = "${var.external_addr}.${var.osp_ipnum}${count.index}"
  }
  
  provisioner "remote-exec" {
    inline = [
      "yum install -y qemu-guest-agent vim"
    ]
  }
}
