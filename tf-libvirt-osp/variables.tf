variable "prjname" {
  default = "taco"
}

variable "osp_ipnum" {
  default = "10"
}

variable "ceph_ipnum" {
  default = "11"
}

variable "deploy_ipnum" {
  default = "5"
}

variable "external_addr" {
  default = "192.168.201"
}

variable "deploy_addr" {
  default = "10.10.10"
}

variable "storage_addr" {
  default = "10.10.20"
}

variable "monitor_addr" {
  default = "10.10.30"
}

variable "internal_addr" {
  default = "10.10.40"
}

variable "disk_pool" {
  default = "default"
}

variable "osp_names" {
  type = list(string)
  default = ["osp-ctl001", "osp-ctl002", "osp-ctl003", "osp-com001", "osp-com002"]
}

variable "ceph_names" {
  type = list(string)
  default = ["ceph001", "ceph002", "ceph003"]
}

variable "deploy_name" {
  default = "deploy"
}

variable "vm_source" {
  default = "/var/lib/libvirt/images/CentOS7-1901.qcow2"
}

variable "fromdisk" {
  default = "CentOS7.qcow2"
}
