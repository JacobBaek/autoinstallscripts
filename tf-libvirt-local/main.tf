resource "libvirt_volume" "emptyvol" {
  name = "empty_vol"
  size = 8589934592
}

resource "libvirt_network" "intnet" {
  name = "int-net"
  mode = "none"
  autostart = true
  bridge = "virbr10"
  addresses = ["172.16.201.0/24"]
}

resource "libvirt_network" "pubnet" {
  name = "pub-net"
  mode = "nat"
  bridge = "virbr11"
  addresses = ["172.16.101.0/24"]
  dns {
    enabled = true
    local_only = true
  }
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
}

data "template_file" "meta_data" {
  template = file("${path.module}/meta_data.yaml")
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config.cfg")

  vars = {
    ip_addr = "172.16.200.200"
    netmask = "24"
    gateway = "172.16.200.1"
  }
}

resource "libvirt_cloudinit_disk" "cloudinit" {
  name = "cloudinit.iso"
  user_data = data.template_file.user_data.rendered
  meta_data = data.template_file.meta_data.rendered
  network_config = data.template_file.network_config.rendered
}

resource "libvirt_volume" "rootvol" {
  name = "root_vol"
  # the place that exist image
  base_volume_pool = "default"
  # the place that will be placed
  pool = "kvmdisk"
  format = "qcow2"
  #source = "/var/lib/libvirt/images/centos7.qcow2"
  base_volume_name = "centos7.qcow2"
}

resource "libvirt_domain" "centos" {
  name = "centos7"
  memory = "4096"
  vcpu = 2
  cloudinit = libvirt_cloudinit_disk.cloudinit.id

  disk {
    volume_id = libvirt_volume.rootvol.id
  }
  
  disk {
    volume_id = libvirt_volume.emptyvol.id
  }

  network_interface {
    network_id = libvirt_network.pubnet.id
    hostname   = "centos7"
    addresses  = ["${var.vm_ipaddr}"]
    wait_for_lease = true
  }

  network_interface {
    network_id = libvirt_network.intnet.id
  }

#  connection {
#    type = "ssh"
#    user = "root"
#    password = "root123"
#    #private_key = "${file("~/.ssh/id_rsa")}"
#    host = var.vm_ipaddr
#    
#  }
#
#  provisioner "remote-exec" {
#    inline = [
#      "yum install vim -y"
#    ]
#  }
}

