#resource "libvirt_volume" "osproot" {
#  count = length(var.master_names)
#  name = "${var.id_name}-osproot-${count.index}"
#  #size = 20000000000
#  pool = var.disk_pool
#  format = "qcow2"
#  source = var.vm_source
#}

resource "libvirt_volume" "masterroot" {
  count = length(var.master_names)
  name = "${var.id_name}-masterroot-${count.index}"
  base_volume_pool = var.disk_pool
  pool = var.disk_pool
  format = "qcow2"
  base_volume_name = var.fromdisk
}

resource "libvirt_volume" "masterdisk" {
  count = length(var.master_names)
  name = "${var.id_name}-masterdisk-${count.index}"
  pool = var.disk_pool
  size = 10000000000
}

resource "libvirt_volume" "workerroot" {
  count = length(var.worker_names)
  name = "${var.id_name}-workerroot-${count.index}"
  base_volume_pool = var.disk_pool
  pool = var.disk_pool
  format = "qcow2"
  base_volume_name = var.fromdisk
}

resource "libvirt_volume" "workerdisk" {
  count = length(var.worker_names)
  name = "${var.id_name}-workerdisk-${count.index}"
  pool = var.disk_pool
  size = 10000000000
}