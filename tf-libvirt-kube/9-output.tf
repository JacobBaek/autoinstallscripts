output "master_instances_with_ipaddr" {
  description = "Kubernetes Master instances with ipaddr"
  value = zipmap(libvirt_domain.master-instances.*.name, libvirt_domain.master-instances[*].network_interface.4.addresses.*)
}

output "worker_instances_with_ipaddr" {
  description = "Kubernetes Master instances with ipaddr"
  value = zipmap(libvirt_domain.worker-instances.*.name, libvirt_domain.worker-instances[*].network_interface.4.addresses.*)
}

output "ceph_instances_with_ipaddr" {
  description = "Ceph instances with ipaddr"
  value = zipmap(libvirt_domain.ceph-instances.*.name, libvirt_domain.ceph-instances.*.network_interface.3.addresses[*])
}
