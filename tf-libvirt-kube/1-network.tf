# It can not make, if you use the duplicated bridge name or network name or addresses.

resource "libvirt_network" "deploy" {
  name = "${var.id_name}-deploy"
  mode = "none"
  bridge = "virbr${var.master_ipnum}0"
  addresses = ["${var.deploy_addr}.0/24"]
}

resource "libvirt_network" "storage" {
  name = "${var.id_name}-storage"
  mode = "none"
  bridge = "virbr${var.master_ipnum}1"
  addresses = ["${var.storage_addr}.0/24"]
}

resource "libvirt_network" "monitor" {
  name = "${var.id_name}-monitor"
  mode = "none"
  bridge = "virbr${var.master_ipnum}2"
  addresses = ["${var.monitor_addr}.0/24"]
}

resource "libvirt_network" "internal" {
  name = "${var.id_name}-internal"
  mode = "none"
  bridge = "virbr${var.master_ipnum}3"
  addresses = ["${var.internal_addr}.0/24"]
}

