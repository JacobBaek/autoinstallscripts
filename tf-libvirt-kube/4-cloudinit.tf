# refer to https://grantorchard.com/dynamic-cloudinit-content-with-terraform-file-templates/

# for user account setting
data "template_file" "user_data" {
  count = length(var.master_names) + length(var.worker_names) + length(var.ceph_names)
  template = file("${path.module}/templates/cloud_init.cfg")
}

# for kube master's Hostname setting
data "template_file" "master_metadata" {
  count = length(var.master_names)
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname    = var.master_names[count.index]
  }
}

# for kube worker's Hostname setting
data "template_file" "worker_metadata" {
  count = length(var.worker_names)
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname    = var.worker_names[count.index]
  }
}

# for Ceph's Hostname setting
data "template_file" "ceph_metadata" {
  count = length(var.ceph_names)
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname    = var.ceph_names[count.index]
  }
}

# for Kube Master's Network setting
data "template_file" "master_network_config" {
  count = length(var.master_names)
  template = file("${path.module}/templates/network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    monitor_addr = "${var.monitor_addr}"
    storage_addr = "${var.storage_addr}"
    internal_addr = "${var.internal_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.master_ipnum}${count.index}"
    netmask = "16"
    gateway = "192.168.0.1"
  }
}

# for Kube Worker's Network setting
data "template_file" "worker_network_config" {
  count = length(var.worker_names)
  template = file("${path.module}/templates/network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    monitor_addr = "${var.monitor_addr}"
    storage_addr = "${var.storage_addr}"
    internal_addr = "${var.internal_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.worker_ipnum}${count.index}"
    netmask = "16"
    gateway = "192.168.0.1"
  }
}

# for Ceph Network setting
data "template_file" "ceph_network_config" {
  count = length(var.ceph_names)
  template = file("${path.module}/templates/ceph_network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    monitor_addr = "${var.monitor_addr}"
    storage_addr = "${var.storage_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.ceph_ipnum}${count.index}"
    netmask = "16"
    gateway = "192.168.0.1"
  }
}
