variable "id_name" {
   default = "jacobbaek"
}

variable "master_ipnum" {
   default = "20"
}

variable "worker_ipnum" {
   default = "21"
}

variable "ceph_ipnum" {
   default = "22"
}

variable "external_addr" {
   default = "192.168.201"
}

variable "deploy_addr" {
   default = "10.10.200"
}

variable "storage_addr" {
   default = "10.10.210"
}

variable "monitor_addr" {
   default = "10.10.220"
}

variable "internal_addr" {
   default = "10.10.230"
}

variable "worker_names" {
   type = list(string)
   default = ["kube-worker001", "kube-worker002"]
}

variable "master_names" {
   type = list(string)
   default = ["kube-master001", "kube-master002", "kube-master003"]
}

variable "ceph_names" {
   type = list(string)
   default = ["ceph001", "ceph002", "ceph003"]
}

variable "vm_source" {
  default = "/var/lib/libvirt/images/centos7.qcow2"
}

variable "fromdisk" {
  default = "centos7.6.1810.qcow2"
}

variable "disk_pool" {
  default = "kvm-images"
}
