resource "libvirt_cloudinit_disk" "worker-cloudinit" {
  count = length(var.worker_names)
  pool = var.disk_pool

  name = "${var.id_name}-worker-cloudinit${count.index}.iso"
  user_data = data.template_file.user_data[count.index].rendered
  meta_data = data.template_file.worker_metadata[count.index].rendered
  network_config = data.template_file.worker_network_config[count.index].rendered
}

resource "libvirt_domain" "worker-instances" {
  count = length(var.worker_names)

  name = format("%s-%s", var.id_name, element(var.worker_names, count.index))
  #name = "${var.worker_ipnum}${count.index}-${var.id_name}"
  memory = "10240"
  vcpu = 4
  # should read https://grantorchard.com/dynamic-cloudinit-content-with-terraform-file-templates/
  cloudinit = libvirt_cloudinit_disk.worker-cloudinit[count.index].id

  disk {
    volume_id = libvirt_volume.workerroot[count.index].id
  }
  
  disk {
    volume_id = libvirt_volume.workerdisk[count.index].id
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }

  # deploy network
  network_interface {
    network_id = libvirt_network.deploy.id
    addresses = ["${var.deploy_addr}.${var.worker_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # storage monitor network
  network_interface {
    network_id = libvirt_network.monitor.id
    addresses = ["${var.monitor_addr}.${var.worker_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # storage network
  network_interface {
    network_id = libvirt_network.storage.id
    addresses = ["${var.storage_addr}.${var.worker_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # internal api network
  network_interface {
    network_id = libvirt_network.internal.id
    addresses = ["${var.internal_addr}.${var.worker_ipnum}${count.index}"]
    wait_for_lease = false
  }

  # external network
  network_interface {
    network_name = "Public-Network"
    addresses = ["${var.external_addr}.${var.worker_ipnum}${count.index}"]
    wait_for_lease = false
  }

  connection {
    type = "ssh"
    user = "root"
    password = "root123"
    host = "${var.external_addr}.${var.worker_ipnum}${count.index}"
  }
  
  provisioner "remote-exec" {
    inline = [
      "yum install -y qemu-guest-agent vim"
    ]
  }
}
